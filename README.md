# Desafio Front-end Multiplicame

Esse desafio tem a finalidade de testar suas habilidades de desenvolver interfaces usando Vue.JS e Quasar Framework.


## Sobre o desafio

- O desafio consiste em uma implementação simples de exibição da lista de funcionalidades que o multiplica.me possúi para membros e gestores.
  
## Layout

Você pode encontrar o Layout com as telas acessando o nosso Figma:

https://www.figma.com/file/UmLQUqdkgEAXYTI4mUQzxt/Multiplica.me-Teste?node-id=0%3A1


1 - Tela ínicial.

2 - Lista de Funcionalidades para gestores.

3 - Lista de Funcionalidades para membros.

4 - Formulário de adição das funcionalidades.


## Setup do projeto

- Quasar v1
- Vue 2

## **API**

Para o seu desafio ser mais interativo, estamos utilizando um mock de API, chamado JSON Server. Portanto, é necessário que você instale-o globalmente em sua máquina para ter os recursos da lib.

**1 -** Como instalar? <br/>
`npm install -g json-server`

**2 -** E para rodar (deixar aberto em uma outra aba do terminal, para que ele fique escutando suas ações de CRUD!), digite o seguinte comando na RAÍZ do projeto: `npm run api`

Link para mais detalhes: https://github.com/typicode/json-server


**Rotas:** <br />
`GET: /managers`<br />
`POST: /managers`<br />
`PUT: /managers`<br />
`PATCH: /managers`<br />
`DELETE: /managers`<br />

`GET: /members` <br />
`POST: /members` <br />
`PUT: /members` <br />
`PATCH: /members` <br />
`DELETE: /members` <br />
<br/>

A API é bem simples, contém uma lista com as funcionalidades que fornecemos tanto para membros quanto para gerentes, no formato id, title(Titulo da Funcionalidade), description(Descrição da Funcionalidade), icon (Nome do Icone que pode ser utilizado);

## **COMO ENTREGAR**

Crie um Repositório no GitHub ou GitLab e envie para gente!



**Orientações e dicas:**

- Dominar o Quasar Framework é um super diferencial, então utilize bem a estrutura e os components que o Quasar Framework fornece. 
  
- É importante que todas as páginas sejam totalmente responsivas.

- Esperamos que você consiga completar o mínimo do desafio; 
  
- Iremos avaliar cada feature, conceito, pattern, validações, tudo o que você fizer de adicional. Tudo além do proposta contará pontos!

- Faça commits regulares. Eles são melhores pra nos ajudar a entender seu tipo de organização.
  
